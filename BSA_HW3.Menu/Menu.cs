﻿using BSA_HW3.Common;
using BSA_HW3.Data.Models;
using BSA_HW3.Menu.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Timers;
using Task = System.Threading.Tasks.Task;

namespace BSA_HW3.Menu
{
    public class Menu
    {
        private ProjectHttpService projectHttpService;
        private TaskHttpService taskHttpService;
        private TeamHttpService teamHttpService;
        private UserHttpService userHttpService;
        public Menu()
        {
            projectHttpService = new ProjectHttpService();
            taskHttpService = new TaskHttpService();
            teamHttpService = new TeamHttpService();
            userHttpService = new UserHttpService();
        }
        public int menu()
        {
            Console.WriteLine("1 - Display operations with projects ");
            Console.WriteLine("2 - Display operations with tasks ");
            Console.WriteLine("3 - Display operations with teams ");
            Console.WriteLine("4 - Display operations with users ");
            Console.WriteLine("99 - exit: ");
            int num = Convert.ToInt32(Console.ReadLine());
            if (num >= 1 && num <= 4)
            {

                if (num == 1)
                {
                    return projectMenu();
                }
                if (num == 2)
                {
                    return taskMenu();
                }
                if (num == 3)
                {
                    return teamMenu();
                }
                if (num == 4)
                {
                    return userMenu();
                }
            }
            else
            {
                throw new ArgumentException();
            }
            return 0;
        }

        public int projectMenu()
        {
            Console.Clear();
            Console.WriteLine("1 - Add project ");
            Console.WriteLine("2 - Display all projects ");
            Console.WriteLine("3 - Display project by id ");
            Console.WriteLine("4 - Update project ");
            Console.WriteLine("5 - Delete project ");
            Console.WriteLine("6 - Get projects with tasks ");
            Console.WriteLine("7 - Get project info ");
            Console.WriteLine("99 - exit: ");
            return Convert.ToInt32(Console.ReadLine());
        }
        public int taskMenu()
        {
            Console.Clear();
            Console.WriteLine("8 - Add task ");
            Console.WriteLine("9 - Display all tasks ");
            Console.WriteLine("10 - Display task by id ");
            Console.WriteLine("11 - Update task ");
            Console.WriteLine("12 - Delete task ");
            Console.WriteLine("13 - Get user tasks ");
            Console.WriteLine("14 - Get finished tasks ");
            Console.WriteLine("28 - Mark random task with delay ");
            Console.WriteLine("99 - exit: ");
            return Convert.ToInt32(Console.ReadLine());
        }
        public int teamMenu()
        {
            Console.Clear();
            Console.WriteLine("15 - Add team ");
            Console.WriteLine("16 - Display all teams ");
            Console.WriteLine("17 - Display team by id ");
            Console.WriteLine("18 - Update team ");
            Console.WriteLine("19 - Delete team ");
            Console.WriteLine("20 - Get sorted team info ");
            Console.WriteLine("99 - exit: ");
            return Convert.ToInt32(Console.ReadLine());
        }
        public int userMenu()
        {
            Console.Clear();
            Console.WriteLine("21 - Add user ");
            Console.WriteLine("22 - Display all users ");
            Console.WriteLine("23 - Display user by id ");
            Console.WriteLine("24 - Update user ");
            Console.WriteLine("25 - Delete user ");
            Console.WriteLine("26 - Get sorted users by name ");
            Console.WriteLine("27 - Get user project info ");
            Console.WriteLine("99 - exit: ");
            return Convert.ToInt32(Console.ReadLine());
        }
        
        public Task<int> MarkRandomTaskWithDelay(int millisecondsTimeout)
        {
            Console.Clear();
            TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();
            Timer timer = new Timer(millisecondsTimeout);
            timer.AutoReset = false;
            timer.Start();
            timer.Elapsed += async delegate
            {
                timer.Dispose();
                tcs.SetResult(await taskHttpService.RandomTaskAsDone());
            };

            tcs = new TaskCompletionSource<int>(timer);
            return tcs.Task;
        }

        public async Task DisplayAllProjects()
        {
            Console.Clear();
            Console.WriteLine("All projects: ");
            foreach(ProjectDTO p in await projectHttpService.GetProjects())
            {
                Console.WriteLine(p.Name + " " + p.CreatedAt);
            }
        }
        public async Task DisplayAllTasks()
        {
            Console.Clear();
            Console.WriteLine("All tasks: ");
            foreach(TaskDTO t in await taskHttpService.GetTasks())
            {
                Console.WriteLine(t.Name + " " + t.TaskState);
            }
        }
        public async Task DisplayAllTeams()
        {
            Console.Clear();
            Console.WriteLine("All teams: ");
            foreach(TeamDTO t in await teamHttpService.GetTeams())
            {
                Console.WriteLine(t.Name + " "+ t.CreatedAt);
            }
        }
        public async Task DisplayAllUsers()
        {
            Console.Clear();
            Console.WriteLine("All users: ");
            foreach ( UserDTO u in await userHttpService.GetUsers())
            {
                Console.WriteLine(u.FirstName + " " + u.LastName + " " + u.Email);
            }
        }
        public async Task DeleteProject()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("Enter project Id you want to delete: ");
                int id = Convert.ToInt32(Console.ReadLine());
                await projectHttpService.DeleteProject(id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.Write("Press Eneter key to continue.. ");
                Console.ReadLine();
            }
        }
        public async Task DeleteTask()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("Enter task Id you want to delete: ");
                int id = Convert.ToInt32(Console.ReadLine());
                await taskHttpService.DeleteTask(id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.Write("Press Eneter key to continue.. ");
                Console.ReadLine();
            }
        }
        public async Task DeleteTeam()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("Enter team Id you want to delete: ");
                int id = Convert.ToInt32(Console.ReadLine());
                await teamHttpService.DeleteTeam(id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.Write("Press Eneter key to continue.. ");
                Console.ReadLine();
            }
        }
        public async Task DeleteUser()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("Enter user Id you want to delete: ");
                int id = Convert.ToInt32(Console.ReadLine());
                await userHttpService.DeleteUser(id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.Write("Press Eneter key to continue.. ");
                Console.ReadLine();
            }
        }
        public async Task DisplayProjectById()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("Enter project Id: ");
                int id = Convert.ToInt32(Console.ReadLine());
                var project = await projectHttpService.GetProjectById(id);
                Console.WriteLine(project.Id + " " + project.Name + " " + project.CreatedAt);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.Write("Press Eneter key to continue.. ");
                Console.ReadLine();
            }
        }
        public async Task DisplayTaskById()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("Enter task Id: ");
                int id = Convert.ToInt32(Console.ReadLine());
                var task = await taskHttpService.GetTaskById(id);
                Console.WriteLine(task.TaskId + " " + task.Name + " " + task.TaskState);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.Write("Press Eneter key to continue.. ");
                Console.ReadLine();
            }
        }
        public async Task DisplayTeamById()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("Enter team Id: ");
                int id = Convert.ToInt32(Console.ReadLine());
                var team = await teamHttpService.GetTeamById(id);
                Console.WriteLine(team.Id + " " + team.Name);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.Write("Press Eneter key to continue.. ");
                Console.ReadLine();
            }
        }
        public async Task DisplayUserById()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("Enter user Id: ");
                int id = Convert.ToInt32(Console.ReadLine());
                var user = await userHttpService.GetUserById(id);
                Console.WriteLine(user.Id + " " + user.FirstName + " " + user.LastName + " " + user.Email);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.Write("Press Eneter key to continue.. ");
                Console.ReadLine();
            }
        }
        public async Task AddProject()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("Enter project name: ");
                string  pName = Console.ReadLine();
                Console.WriteLine("Enter project description: ");
                string pDescription = Console.ReadLine();
                Console.WriteLine("Enter project deadline: ");
                DateTime deadline = Convert.ToDateTime(Console.ReadLine());
                Console.WriteLine("Enter task performer id: ");
                int userId = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter project team id: ");
                int teamId = Convert.ToInt32(Console.ReadLine());
                var pdto = new ProjectDTO {
                    Id = 0,
                    Name = pName,
                    Descriprion = pDescription,
                    AuthorId = userId,
                    TeamId = teamId,
                    CreatedAt = DateTime.Now,
                    Deadline = deadline
                };
                await projectHttpService.CreateProject(pdto);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.Write("Press Eneter key to continue.. ");
                Console.ReadLine();
            }
        }

        public async Task AddTask()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("Enter task name: ");
                string tName = Console.ReadLine();
                Console.WriteLine("Enter task description: ");
                string tDescription = Console.ReadLine();
                Console.WriteLine("Enter project id: ");
                int pId = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter task performer id: ");
                int userId = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter project team id: ");
                int teamId = Convert.ToInt32(Console.ReadLine());
                TaskState taskState = new TaskState();
                Console.WriteLine("Enter 1-4, choose task state: ");
                Console.WriteLine("1 - To do");
                Console.WriteLine("2 - In progress");
                Console.WriteLine("3 - Done");
                Console.WriteLine("4 - Cancelled");
                int num = Convert.ToInt32(Console.ReadLine());
                if (num >= 1 && num <= 4)
                {
                    if (num == 1) taskState = TaskState.ToDo;
                    if (num == 2) taskState = TaskState.InProgress;
                    if (num == 3) taskState = TaskState.Done;
                    if (num == 4) taskState = TaskState.Canceled;
                }
                else
                {
                    throw new ArgumentException();
                }
                var tdto = new TaskDTO
                {
                    TaskId = 0,
                    TaskState = taskState,
                    CreatedAt = DateTime.Now,
                    Description = tDescription,
                    Name = tName,
                    PerformerId = userId,
                    ProjectId = pId
                };
                await taskHttpService.CreateTask(tdto);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.Write("Press Eneter key to continue.. ");
                Console.ReadLine();
            }
        }
        public async Task AddTeam()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("Enter team name: ");
                string tName = Console.ReadLine();
                var tdto = new TeamDTO
                {
                    Id = 0,
                    CreatedAt = DateTime.Now,
                    Name = tName
                };
                await teamHttpService.CreateTeam(tdto);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.Write("Press Eneter key to continue.. ");
                Console.ReadLine();
            }
        }
        public async Task AddUser()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("Enter user first name: ");
                string uFName = Console.ReadLine();
                Console.WriteLine("Enter user second name: ");
                string uSName = Console.ReadLine();
                Console.WriteLine("Enter user email: ");
                string uEmail = Console.ReadLine();
                Console.WriteLine("Enter user birthday: ");
                DateTime bDay = Convert.ToDateTime(Console.ReadLine());
                Console.WriteLine("Enter team id: ");
                int teamId = Convert.ToInt32(Console.ReadLine());
                var udto = new UserDTO
                {
                    Id = 0,
                    FirstName = uFName,
                    LastName = uSName,
                    Email = uEmail,
                    BirthDay = bDay,
                    TeamId = teamId,
                };
                await userHttpService.CreateUser(udto);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.Write("Press Eneter key to continue.. ");
                Console.ReadLine();
            }
        }

        public async Task UpdateProject()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("Enter project id you want to update: ");
                int id = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter new project name: ");
                string pName = Console.ReadLine();
                Console.WriteLine("Enter new project description: ");
                string pDescription = Console.ReadLine();
                Console.WriteLine("Enter new project deadline: ");
                DateTime deadline = Convert.ToDateTime(Console.ReadLine());
                Console.WriteLine("Enter new task performer id: ");
                int userId = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter new project team id: ");
                int teamId = Convert.ToInt32(Console.ReadLine());
                var pdto = new ProjectDTO
                {
                    Id = id,
                    Name = pName,
                    Descriprion = pDescription,
                    AuthorId = userId,
                    TeamId = teamId,
                    Deadline = deadline
                };
                await projectHttpService.UpdateProject(pdto);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.Write("Press Eneter key to continue.. ");
                Console.ReadLine();
            }
        }

        public async Task UpdateTask()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("Enter task id you want to change: ");
                int taskId = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter new task name: ");
                string tName = Console.ReadLine();
                Console.WriteLine("Enter new task description: ");
                string tDescription = Console.ReadLine();
                Console.WriteLine("Enter new project id: ");
                int pId = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter new task performer id: ");
                int userId = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter new project team id: ");
                int teamId = Convert.ToInt32(Console.ReadLine());
                TaskState taskState = new TaskState();
                Console.WriteLine("Enter 1-4, choose new task state: ");
                Console.WriteLine("1 - To do");
                Console.WriteLine("2 - In progress");
                Console.WriteLine("3 - Done");
                Console.WriteLine("4 - Cancelled");
                int num = Convert.ToInt32(Console.ReadLine());
                if (num >= 1 && num <= 4)
                {
                    if (num == 1) taskState = TaskState.ToDo;
                    if (num == 2) taskState = TaskState.InProgress;
                    if (num == 3) taskState = TaskState.Done;
                    if (num == 4) taskState = TaskState.Canceled;
                }
                else
                {
                    throw new ArgumentException();
                }
                var tdto = new TaskDTO
                {
                    TaskId = taskId,
                    TaskState = taskState,
                    CreatedAt = DateTime.Now,
                    Description = tDescription,
                    Name = tName,
                    PerformerId = userId,
                    ProjectId = pId
                };
                await taskHttpService.UpdateTask(tdto);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.Write("Press Eneter key to continue.. ");
                Console.ReadLine();
            }
        }
        public async Task UpdateTeam()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("Enter team id you want to change: ");
                int teamId = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter new team name: ");
                string tName = Console.ReadLine();
                var tdto = new TeamDTO
                {
                    Id = teamId,
                    Name = tName
                };
                await teamHttpService.UpdateTeam(tdto);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.Write("Press Eneter key to continue.. ");
                Console.ReadLine();
            }
        }

        public async Task UpdateUser()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("Enter user id you want to update: ");
                int uid = Convert.ToInt32(Console.ReadLine());
                var user = await userHttpService.GetUserById(uid);
                Console.WriteLine("Enter new user first name: ");
                string uFName = Console.ReadLine();
                Console.WriteLine("Enter new user second name: ");
                string uSName = Console.ReadLine();
                Console.WriteLine("Enter new user email: ");
                string uEmail = Console.ReadLine();
                Console.WriteLine("Enter new user birthday: ");
                DateTime bDay = Convert.ToDateTime(Console.ReadLine());
                Console.WriteLine("Enter new team id: ");
                int teamId = Convert.ToInt32(Console.ReadLine());
                var udto = new UserDTO
                {
                    Id = user.Id,
                    FirstName = uFName,
                    LastName = uSName,
                    Email = uEmail,
                    BirthDay = bDay,
                    TeamId = teamId,
                };
                await userHttpService.UpdateUser(udto);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.Write("Press Eneter key to continue.. ");
                Console.ReadLine();
            }
        }
        public async Task DisplayProjectToTasksCountDictionary()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("Enter author Id: ");
                int id = Convert.ToInt32(Console.ReadLine());
                foreach( KeyValuePair<ProjectDTO, int> kvp 
                    in await projectHttpService.GetProjectToTasksCountDictionary(id))
                {
                    Console.WriteLine("Project = {0}, Count = {1}", kvp.Key, kvp.Value);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.Write("Press Eneter key to continue.. ");
                Console.ReadLine();
            }
        }
        public async Task DisplayProjectInfo()
        {
            Console.Clear();
            Console.WriteLine("Project info: ");
            foreach (ProjectInfo p in await projectHttpService.GetProjectInfo())
            {
                Console.WriteLine(p.Project.Name + " Project team count: "
                        + p.ProjectTeamCount + " Shortest name task: " + p.ShortestNameTask.Name
                        + " Longest descroption task: " + p.LongestDescriptionTask.Name);
            }
        }

        public async Task DisplayUserTasks()
        {
            Console.Clear();
            Console.WriteLine("Enter user Id: ");
            int id = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("User tasks: ");
            foreach (TaskDTO t in await taskHttpService.GetUserTasks(id))
            {
                Console.WriteLine(t.Name + " " + t.TaskState);
            }
        }
        public async Task DisplayFinishedTaskIdName()
        {
            Console.Clear();
            Console.WriteLine("Enter user Id: ");
            int id = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Finished tasks: ");
            foreach (TaskInfo t in await taskHttpService.GetFinishedTaskIdName(id))
            {
                Console.WriteLine(t.Id + " " + t.Name);
            }
        }
        public async Task DisplaySortedTeamsInfo()
        {
            Console.Clear();
            Console.WriteLine("Sorted teams: ");
            foreach (TeamInfo t in await teamHttpService.GetSortedTeamsInfo())
            {
                Console.WriteLine(t.Id + " " + t.Name);
            }
        }
        public async Task DisplayUsersSortedByName()
        {
            Console.Clear();
            Console.WriteLine("User sorted: ");
            foreach (UserDTO u in await userHttpService.GetUsersSortedByName())
            {
                Console.WriteLine(u.FirstName + " " + u.LastName + " " + u.Email);
            }
        }
        public async Task DisplayUserProjectInfo()
        {
            Console.Clear();
            Console.WriteLine("Enter user Id: ");
            int id = Convert.ToInt32(Console.ReadLine());
            UserProjectInfo upi = await userHttpService.GetUserProjectInfo(id);
            Console.WriteLine("All tasks count: " + upi.AllTasksCount
                + "Longest task: " + upi.LongestTask.TaskName 
                + " Last created project: " + upi.LastCreatedProject.ProjectName
                + " Cancelled or in progress: " + upi.CancelledOrInProgressTasksCount);
        }
    }
}
