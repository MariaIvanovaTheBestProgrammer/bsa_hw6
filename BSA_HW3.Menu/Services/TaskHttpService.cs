﻿using BSA_HW3.Common;
using BSA_HW3.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace BSA_HW3.Menu.Services
{
    public class TaskHttpService
    {
        static readonly HttpClient client = new HttpClient();

        public async Task<IEnumerable<TaskDTO>> GetTasks()
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(Settings.basePath + "task");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<TaskDTO>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return null;
            }
        }
        public async Task<TaskDTO> GetTaskById(int taskId)
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(Settings.basePath + $"task/{taskId}");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<TaskDTO>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return null;
            }
        }
        public async Task<IEnumerable<TaskDTO>> GetUserTasks(int userId)
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(Settings.basePath + $"task/UserTasks/{userId}");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<TaskDTO>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return null;
            }
        }
        public async Task<IEnumerable<TaskInfo>> GetFinishedTaskIdName(int userId)
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(Settings.basePath + $"task/FinishedTasks/{userId}");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<TaskInfo>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return null;
            }
        }
        public async Task CreateTask(TaskDTO taskDTO)
        {
            try
            {
                var stringContent = new StringContent(JsonConvert.SerializeObject(taskDTO), Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(Settings.basePath + "task", stringContent);
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }
        }
        public async Task DeleteTask(int taskId)
        {
            try
            {
                HttpResponseMessage response = await client.DeleteAsync(Settings.basePath + $"task/{taskId}");
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }
        }
        public async Task UpdateTask(TaskDTO taskDTO)
        {
            try
            {
                var stringContent = new StringContent(JsonConvert.SerializeObject(taskDTO), Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PutAsync(Settings.basePath + "task", stringContent);
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }
        }

        public async Task<int> RandomTaskAsDone()
        {
            try
            {
                HttpResponseMessage response1 = await client.GetAsync(Settings.basePath + $"task/randomtask");
                response1.EnsureSuccessStatusCode();
                string responseBody = await response1.Content.ReadAsStringAsync();
                var task = JsonConvert.DeserializeObject<TaskDTO>(responseBody);
                HttpResponseMessage response2 = await client.PutAsync(Settings.basePath + $"task/donetask/{task.TaskId}", null);
                response2.EnsureSuccessStatusCode();
                return task.TaskId;
            }
            catch(AggregateException e)
            {
                throw;
            }
        }
    }
}
