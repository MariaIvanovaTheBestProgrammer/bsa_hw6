﻿using BSA_HW3.Data.Interfaces;
using BSA_HW3.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace BSA_HW3.Data.Repositories
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly DatabaseContext _context;

        public ProjectRepository(DatabaseContext context)
        {
            _context = context;
        }

        public async Task CreateProject(Project project)
        {
            await _context.AddAsync(project);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteProject(int projectId)
        {
            var entity = await _context.Projects.Where(p => p.Id == projectId).FirstOrDefaultAsync();
            _context.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<Project> GetProjectById(int projectId)
        {
            return await _context.Projects.Where(e => e.Id == projectId).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Project>> GetProjects()
        {
            return await _context.Projects.ToListAsync();
        }

        public async Task UpdateProject(Project project)
        {
            var tmp = await _context.Projects.FirstOrDefaultAsync(e => e.Id == project.Id);
            if (tmp != null)
            {
                tmp.ProjectName = project.ProjectName;
                tmp.TeamId = project.TeamId;
                tmp.Team = project.Team;
                tmp.UserId = project.UserId;
                tmp.User = project.User;
                tmp.CreatedAt = project.CreatedAt;
                tmp.Deadline = project.Deadline;
                tmp.Descriprion = project.Descriprion;
            }
            await _context.SaveChangesAsync();
        }

    }

}
