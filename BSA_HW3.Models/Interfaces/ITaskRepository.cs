﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BSA_HW3.Data.Models;

namespace BSA_HW3.Data.Interfaces
{
    public interface ITaskRepository
    {
        Task<IEnumerable<Models.Task>> GetTasks();
        Task<Models.Task> GetTaskById(int taskId);
        System.Threading.Tasks.Task CreateTask(Models.Task task);
        System.Threading.Tasks.Task DeleteTask(int taskId);
        System.Threading.Tasks.Task UpdateTask(Models.Task task);
        System.Threading.Tasks.Task MarkTaskAsDone(int taskId);
        Task<Models.Task> GetRandomTask();
    }
}
