﻿using BSA_HW3.BusinessLogic.Interfaces;
using BSA_HW3.Common;
using BSA_HW3.Data.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BSA_HW3.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectDTO>>> GetProjects()
        {
            return Ok(await _projectService.GetProjects());
        }
        [HttpGet("{projectId}")]
        public async Task<ActionResult<ProjectDTO>> GetProjectById(int projectId)
        {
            try
            {
                return Ok(await _projectService.GetProjectById(projectId));
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }
        [HttpDelete("{projectId}")]
        public async Task<IActionResult> DeleteProject(int projectId)
        {
            try
            {
                await _projectService.DeleteProject(projectId);
                return NoContent();
            }catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }
        [HttpPost]
        public async Task<IActionResult> CreateProject(ProjectDTO projectDTO)
        {
            return StatusCode(201, await _projectService.CreateProject(projectDTO));
        }
        [HttpPut]
        public async Task<IActionResult> UpdateProject(ProjectDTO projectDTO)
        {
            await _projectService.UpdateProject(projectDTO);
            return Ok(await _projectService.GetProjectById(projectDTO.Id));
        }
        [HttpGet("ProjectToTasksCount/{authorId}")]
        public async Task<ActionResult<IEnumerable<KeyValuePair<Project, int>>>> GetProjectToTasksCountDictionary(int authorId)
        {
            return Ok(await _projectService.GetProjectToTasksCountDictionary(authorId));
        }
        [HttpGet("Info")]
        public async Task<ActionResult<IEnumerable<ProjectInfo>>> GetProjectInfo()
        {
            return Ok(await _projectService.GetProjectInfo());
        }

    }
}
