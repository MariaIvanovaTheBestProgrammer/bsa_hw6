﻿using BSA_HW3.BusinessLogic.Interfaces;
using BSA_HW3.Common;
using BSA_HW3.Data.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BSA_HW3.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly ITeamService _teamService;

        public TeamController(ITeamService teamService)
        {
            _teamService = teamService;
        }
        [HttpGet("teams")]
        public async Task<ActionResult<IEnumerable<TeamDTO>>> GetTeams()
        {
            return Ok(await _teamService.GetTeams());
        }
        [HttpGet("{teamId}")]
        public async Task<ActionResult<TeamDTO>> GetTeamById(int teamId)
        {
            return Ok(await _teamService.GetTeamById(teamId));
        }
        [HttpDelete("{teamId}")]
        public async Task<IActionResult> DeleteTeam(int teamId)
        {
            await _teamService.DeleteTeam(teamId);
            return NoContent();
        }
        [HttpPost]
        public async Task<IActionResult> CreateTeam(TeamDTO teamDTO)
        {
            return StatusCode(201, await _teamService.CreateTeam(teamDTO));
        }
        [HttpPut]
        public async Task<IActionResult> UpdateTeam(TeamDTO teamDTO)
        {
            await _teamService.UpdateTeam(teamDTO);
            return Ok(await _teamService.GetTeamById(teamDTO.Id));
        }
        [HttpGet("SortedTeams")]
        public async Task<ActionResult<IEnumerable<TeamInfo>>> GetSortedTeamsInfo()
        {
            return Ok(await _teamService.GetSortedTeamsInfo());
        }
    }
}
