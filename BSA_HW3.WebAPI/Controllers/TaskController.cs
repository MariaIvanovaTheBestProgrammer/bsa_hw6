﻿using BSA_HW3.BusinessLogic.Interfaces;
using BSA_HW3.Common;
using BSA_HW3.Data.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BSA_HW3.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly ITaskService _taskService;

        public TaskController(ITaskService taskService)
        {
            _taskService = taskService;
        }
        [HttpGet("tasks")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetTasks()
        {
            return Ok(await _taskService.GetTasks());
        }
        [HttpGet("{taskId}")]
        public async Task<ActionResult<TaskDTO>> GetTaskById(int taskId)
        {
            return Ok(await _taskService.GetTaskById(taskId));
        }
        [HttpDelete("{taskId}")]
        public async Task<IActionResult> DeleteTask(int taskId)
        {
            await _taskService.DeleteTask(taskId);
            return NoContent();
        }
        [HttpPost]
        public async Task<IActionResult> CreateTask(TaskDTO taskDTO)
        {
            await _taskService.CreateTask(taskDTO);
            return StatusCode(201, await _taskService.GetTaskById(taskDTO.TaskId));
        }
        [HttpPut]
        public async Task<IActionResult> UpdateTask(TaskDTO taskDTO)
        {
            await _taskService.UpdateTask(taskDTO);
            return Ok(await _taskService.GetTaskById(taskDTO.TaskId));
        }
        [HttpGet("UserTasks")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetUserTasks(int userId)
        {
            return Ok(await _taskService.GetUserTasks(userId));
        }
        [HttpGet("FinishedTasks")]
        public async Task<ActionResult<IEnumerable<TaskInfo>>> GetFinishedTaskIdName(int userId)
        {
            return Ok(await _taskService.GetFinishedTaskIdName(userId));
        }

        [HttpPut("DoneTask/{taskId}")]
        public async Task<IActionResult> MarkTaskAsDone(int taskId)
        {
            await _taskService.MarkTaskAsDone(taskId);
            return Ok(await _taskService.GetTaskById(taskId));
        }

        [HttpGet("RandomTask")]
        public async Task<ActionResult<TaskDTO>> GetRandomTask()
        {
            return Ok(await _taskService.GetRandomTask());
        }

    }
}
