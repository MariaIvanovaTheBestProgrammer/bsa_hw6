﻿using AutoMapper;
using BSA_HW3.BusinessLogic.Interfaces;
using BSA_HW3.Common;
using BSA_HW3.Data.Interfaces;
using BSA_HW3.Data.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace BSA_HW3.BusinessLogic.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IProjectRepository _projectRepository;
        private readonly ITaskRepository _taskRepository;
        private readonly ITeamRepository _teamRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _iMapper;

        public ProjectService(IProjectRepository projectRepository, 
                              ITaskRepository taskRepository,
                              ITeamRepository teamRepository,
                              IUserRepository userRepository,
                              IMapper mapper)
        {
            _projectRepository = projectRepository;
            _taskRepository = taskRepository;
            _teamRepository = teamRepository;
            _userRepository = userRepository;
            _iMapper = mapper;
        }

        public async Task<ProjectDTO> CreateProject(ProjectDTO projectDTO)
        {
            var entity = _iMapper.Map<ProjectDTO, Project>(projectDTO);
            await _projectRepository.CreateProject(entity);
            return _iMapper.Map<Project, ProjectDTO>(entity);
        }

        public async Task DeleteProject(int projectId)
        {
            await _projectRepository.DeleteProject(projectId);
        }

        public async Task<ProjectDTO> GetProjectById(int projectId)
        {
            return _iMapper.Map<ProjectDTO>(await _projectRepository.GetProjectById(projectId));
        }

        public async Task<IEnumerable<ProjectDTO>> GetProjects()
        {
            var projects = await _projectRepository.GetProjects();
            return projects.Select(e => _iMapper.Map<ProjectDTO>(e));
        }

        public async Task UpdateProject(ProjectDTO projectDTO)
        {
            await _projectRepository.UpdateProject(_iMapper.Map<ProjectDTO, Project>(projectDTO));
        }

        //1 linq
        public async Task<IEnumerable<KeyValuePair<ProjectDTO, int>>> GetProjectToTasksCountDictionary(int authorId)
        {
            var projects = await _projectRepository.GetProjects();
            var tasks = await _taskRepository.GetTasks();
            projects = projects.GroupJoin(
                                     tasks,
                                     p => p.Id,
                                     t => t.ProjectId,
                                     (p, t) =>
                                     {
                                         p.Tasks = t;
                                         return p;
                                     }
                           );
            return projects.Where(p => p.UserId == authorId)
                           .ToDictionary(x => _iMapper.Map<ProjectDTO>(x), x => x.Tasks?.Count() ?? 0)
                           .ToArray();
        }

        //7 linq
        public async Task<IEnumerable<ProjectInfo>> GetProjectInfo()
        {
            var projects = await _projectRepository.GetProjects();
            var tasks = await _taskRepository.GetTasks();
            var teams = await _teamRepository.GetTeams();
            var users = await _userRepository.GetUsers();
            projects = projects.GroupJoin(
                                      tasks,
                                      p => p.Id,
                                      t => t.ProjectId,
                                      (p, t) =>
                                      {
                                          p.Tasks = t;
                                          return p;
                                      }
                            )
                            .Join(
                                    teams.GroupJoin(
                                                    users,
                                                    t => t.Id,
                                                    u => u.TeamId,
                                                    (t, u) =>
                                                    {
                                                        t.Users = u;
                                                        return t;
                                                    }
                                                    ),
                                    p => p.TeamId,
                                    t => t.Id,
                                    (p, t) =>
                                    {
                                        p.Team = t;
                                        return p;
                                    }
                            );
            return projects.Where(p => p.Descriprion.Length > 20 || p.Tasks.Count() < 3)
                .Select(p => new ProjectInfo
                {
                    Project = _iMapper.Map<ProjectDTO>(p),
                    LongestDescriptionTask = _iMapper.Map<TaskDTO>(p.Tasks.OrderByDescending(t => t.Description).FirstOrDefault()),
                    ShortestNameTask = _iMapper.Map<TaskDTO>(p.Tasks.OrderBy(t => t.TaskName).FirstOrDefault()),
                    ProjectTeamCount = p.Team.Users.Count()
                });
        }
    }
}
