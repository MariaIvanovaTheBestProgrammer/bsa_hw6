﻿using BSA_HW3.Common;
using BSA_HW3.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace BSA_HW3.BusinessLogic.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<UserDTO>> GetUsers();
        Task<UserDTO> GetUserById(int userId);
        Task<UserDTO> CreateUser(UserDTO userDTO);
        Task DeleteUser(int userId);
        Task UpdateUser(UserDTO userDTO);
        Task<IEnumerable<User>> GetUsersSortedByName();
        Task<UserProjectInfo> GetUserProjectInfo(int userId);
    }
}
